@extends('adminlte.master')

@section('judul')
  Buat Account Baru!
@endsection

@section('content')
  <h2>Sign Up Form</h2>

  <form method="POST" action="{{ url('welcome')  }}">
    @csrf
    <label for="firstName">First Name:</label><br>
    <input type="text" id="firstName" name="firstName" required><br><br>

    <label for="lastName">Last Name:</label><br>
    <input type="text" id="lastName" name="lastName"><br><br>

    <label>Gender:</label><br><br>
    <input type="radio" name="gender" value="Man" id="man" required>
    <label for="man">Man</label><br>

    <input type="radio" name="gender" value="Woman" id="woman">
    <label for="woman">Woman</label><br>

    <input type="radio" name="gender" value="Other" id="other">
    <label for="other">Other</label><br><br>

    <label for="nationality">Nationality : </label>
    <select name="nationality" id="nationality" required>
      <option value="Indonesia">Indonesian</option>
      <option value="Singaporean">Singaporean</option>
      <option value="Malaysian">Malaysian</option>
      <option value="Australian">Australian</option>
    </select><br><br>

    <label>Language Spoken:</label><br>
    <input type="checkbox" name="language" value="Bahasa Indonesia" id="indonesia">
    <label for="indonesia">Bahasa Indonesia</label><br>

    <input type="checkbox" name="language" value="English" id="english">
    <label for="english">English</label><br>

    <input type="checkbox" name="language" value="Arabic" id="arabic">
    <label for="arabic">Arabicr</label><br>

    <input type="checkbox" name="language" value="Japanese" id="japanese">
    <label for="japanese">Japanese</label><br><br>

    <label for="bio">Bio</label><br><br>
    <textarea name="bio" id="bio" cols="30" rows="10" required></textarea><br>

    <input type="submit" value="Sign Up">
  </form>
@endsection
