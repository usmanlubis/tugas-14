@extends('adminlte.master')

@section('judul')
    SanberBook
@endsection

@section('content')
    <h2>Social Media Developer Santai Berkualitas</h2>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
    <h3>Benefit Join SanberBook</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Diaplikasikan dalam bentuk kode</li>
        <li>Menjadi bagian dari kemajuan SanberBook</li>
    </ul>
    <h3>Cara Bergabung ke SanberBook</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
        <li>Selesai!</li>
    </ol>
@endsection
