@extends('adminlte.master')

@section('judul')
  Tambah Cast
@endsection

@section('content')
  <form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control" id="nama" name="nama">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="umur">Umur</label>
      <input type="text" class="form-control" id="umur" name="umur">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="bio">Bio</label>
      <textarea name="bio" id="bio" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection