@extends('adminlte.master')

@section('judul')
  Edit Cast
@endsection

@section('content')
  <form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control" id="nama" name="nama" value="{{$cast->nama}}">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="umur">Umur</label>
      <input type="text" class="form-control" id="umur" name="umur" value="{{$cast->umur}}">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="bio">Bio</label>
      <textarea name="bio" id="bio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection