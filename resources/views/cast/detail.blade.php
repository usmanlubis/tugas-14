@extends('adminlte.master')

@section('judul')
  Detail Cast
@endsection

@section('content')
  <h1 style="text-align: center">{{$cast->nama}}</h1>
  <h3>Umur : {{$cast->umur}} Tahun</h3>
  <h2>Bio</h2>
  <p>{{$cast->bio}}</p>
@endsection