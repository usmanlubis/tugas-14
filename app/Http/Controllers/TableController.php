<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function table()
    {
        return view('items.table');
    }

    public function data_table()
    {
        return view('items.data-table');
    }
}
