<?php

Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@register');
Route::get('/welcome', 'AuthController@welcome');
Route::post('/welcome', 'AuthController@welcome_post');
Route::get('/table', 'TableController@table');
Route::get('/data-table', 'TableController@data_table');

// CRUD Tugas 15 Tabel Cast

// Create Data
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');

// Read Data
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');

// Update Data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');

// Hapus Data
Route::delete('/cast/{cast_id}', 'CastController@destroy');